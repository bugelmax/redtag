<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; <?=get_option('blog_charset')?>">
    <meta name="keywords" content=""/>
    <meta name="description" content="<?=get_the_title().get_the_content()?>"/>
    <meta name="author" content="http://redtag.com.ua/">
    <meta property="og:image" content="/img/logo_b.png">
    <meta property="og:image:type" content="/image/png">
    <meta property="og:image:width" content="345">
    <meta property="og:image:height" content="95">
    <meta name="og:description" content="<?=get_the_title().get_the_content()?>"/>
    <meta name="theme-color" content="#ffffff">
    <link rel="shortcut icon" href="/img/favicon/favicon.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Red Tag - <?=get_the_title()?></title>
<!--    <link rel="stylesheet" href="--><?//=get_template_directory_uri()?><!--/public/css/style.min.css">-->
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/app/css/style.min.css">
<?php wp_head(); ?>
</head>
<body>
    <header class="sticky">
        <div class="container">
            <div class="logo">
                <a href="/"><img src="<?=get_template_directory_uri()?>/public/img/header/header_logo.svg" alt=""></a>
            </div>
            <div class="burger-menu">
                <div class="burger"></div>
            </div>
            <?php wp_nav_menu( array( 'container_class' => 'menu', 'menu' => 'header-menu-1' ) );?>
            <div class="send">
                <img src="<?=get_template_directory_uri()?>/public/img/header/envelope.png" alt="">
                <span>Contact Us</span>
            </div>
            <div class="mobile_menu">
                <?php wp_nav_menu( array( 'container_class' => 'menu', 'menu' => 'header-menu-1' ) );?>
                <div class="send">
                    <img src="<?=get_template_directory_uri()?>/public/img/header/envelope.png" alt="">
                    <span>Contact Us</span>
                </div>
                <nav class="social_contact">
                    <a href="#" target="_blank" class="social_link fb"></a>
                    <a href="#" target="_blank" class="social_link in"></a>
                    <a href="#" target="_blank" class="social_link ins"></a>
                    <a href="#" target="_blank" class="social_link tw"></a>
                </nav>
            </div>
        </div>
    </header>
