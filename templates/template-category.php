<?php
get_header();
$template_url=get_template_directory_uri();
$args = array(
    'numberposts' => 13,

    'orderby'     => 'date',
    'order'       => 'DESC',
    'include'     => array(),
    'exclude'     => array(),
    'post_type'   => 'post',
    'suppress_filters' => false,
);
$posts=get_posts($args);

?>

<section class="projects wow animated fadeInUp">
    <div class="container">
        <div class="title_block">
            <h2 class="title_text">Our<br/>projects</h2>
            <span class="we_are">what <span class="red">we</span> do.</span>
        </div>
        <nav class="our_skills">
            <div class="our_skills__item active">
                <img src="<?=get_template_directory_uri()?>/public/img/projects/image.png" alt="All projects" class="our_skills__item_img">
                <span class="our_skills__item_title">All projects</span>
            </div>
            <div class="our_skills__item">
                <img src="<?=get_template_directory_uri()?>/public/img/projects/smartphone-1.png" alt="Mobile" class="our_skills__item_img">
                <span class="our_skills__item_title">Mobile</span>
            </div>
            <div class="our_skills__item">
                <img src="<?=get_template_directory_uri()?>/public/img/projects/marketing.png" alt="Web" class="our_skills__item_img">
                <span class="our_skills__item_title">Web</span>
            </div>
            <div class="our_skills__item">
                <img src="<?=get_template_directory_uri()?>/public/img/projects/bar-chart-1.png" alt="Analytics" class="our_skills__item_img">
                <span class="our_skills__item_title">Analytics</span>
            </div>
            <div class="our_skills__item">
                <img src="<?=get_template_directory_uri()?>/public/img/projects/startup.png" alt="Something New" class="our_skills__item_img">
                <span class="our_skills__item_title">Something New</span>
            </div>
        </nav>
    </div>
</section>
<section class="projects_list">
    <div class="container">
        <?php $post_counter=1; foreach ($posts as $obj):?>
            <div class="projects_list__item">
                <div class="projects_list__item_description wow animated fadeIn<?=(($post_counter%2)==($post_counter/2))?'Left':'Right'?>">
                    <div class="projects_list__item_name">
                        <span class="projects_list__item_number">0<?=$post_counter?></span>
                        <span class="projects_list__item_title"><?php echo $obj->post_title;?></span>
                    </div>
                    <div class="projects_list__item_description">
                        <span class="projects_list__item_excerpt"><?php echo do_excerpt($obj->post_excerpt, 200); ?></span>
                    </div>
                    <?php
                    $post_tags = get_the_terms( $obj->ID, 'post_tag' );
                    $html = '<div class="projects_list__item_tags">';
                    foreach ($post_tags as $tag){
                        $tag_link = get_tag_link($tag->term_id);

                        $html .= "<a href='{$tag_link}' title='{$tag->name} Tag' class='projects_list__item_tags_name {$tag->slug}'>";
                        $html .= "{$tag->name}</a>";
                    }
                    $html .= '</div>';
                    echo $html;
                    ?>
                    <div class="buttons"><a href="<?=get_permalink($obj->ID)?>" class="btn">Learn more</a></div>
                </div>
                <div class="projects_list__item_img wow animated fadeInRight">
                    <?php the_post_thumbnail(); ?>
                </div>
            </div>

            <?php
            $post_counter++;
        endforeach;
        ?>
    </div>
</section>
<?php get_footer(); ?>
