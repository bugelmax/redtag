<section class="about about_page wow animated fadeInUp">
    <div class="container">
        <div class="title_block">
            <h2 class="title_text">About <br/>Company</h2>
            <span class="we_are">who <span class="red">we</span> are</span>
        </div>
        <div class="block_text">
            <span class="parag"><?php the_field('about_company'); ?></span>
        </div>
    </div>
</section>

<section class="slider">
    <div class="container">
        <div class="about_slider">
            <?php

            $images = get_field('slider');
            $size = 'full'; // (thumbnail, medium, large, full or custom size)

            if( $images ):
            foreach( $images as $image ):?>
                <div class="about_slider__item">
                    <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                </div>
            <?php endforeach;
            endif; ?>

        </div>
    </div>
</section>

<section class="mission ">
    <div class="container">
        <div class="title_block">
            <h2 class="title_text wow animated fadeInLeft">Our <br/>mission</h2>
            <span class="we_are wow animated fadeInRight">who <span class="red">we</span> are</span>
        </div>
        <div class="block_text wow animated fadeInUp">
            <span class="parag"><?php the_field('our_mission'); ?></span>
        </div>
    </div>
</section>

<section class="contacts wow animated fadeInUp">
    <div class="container">
        <div class="title_block">
            <h2 class="title_text">our <br/>contacts</h2>
            <span class="we_are">who <span class="red">we</span> are</span>
        </div>
        <div class="block_text">
            <div class="our_contact">
                <div class="physical_contact">
                    <span class="sub_title">Where to find us</span>
                    <div>
                        <span class="physical_contact__title">Adress:</span>
                        <div class="parag">
                            <span><?php echo get_theme_mod('redtag_adress_Ukraine', '/'); ?></span>
                            <span><?php echo get_theme_mod('redtag_adress_Netherlands', '/'); ?></span>
                        </div>
                        <span class="physical_contact__title">Email:</span>
                        <a class="parag" href="mailto:<?php echo get_theme_mod('redtag_email', '/'); ?>"><?php echo get_theme_mod('redtag_email', '/'); ?></a>
                        <span class="physical_contact__title">Phone:</span>
                        <div class="parag">
                            <a class=""  href="tel:<?php echo get_theme_mod('redtag_phone_ukr', '/'); ?>">
                                <?php echo get_theme_mod('redtag_phone_ukr','/'); ?>
                            </a>
                            <a class=""  href="tel:<?php echo get_theme_mod('redtag_phone_nd', '/'); ?>">
                                <?php echo get_theme_mod('redtag_phone_nd','/'); ?>
                            </a>
                        </div>
                    </div>
                </div>
                <nav class="social_contact">
                    <a onclick="return !window.open(this.href)" href="<?php echo get_theme_mod('redtag_link_facebook'); ?>" class="social_link fb"></a>
                    <a onclick="return !window.open(this.href)" href="<?php echo get_theme_mod('redtag_link_linkedin'); ?>" class="social_link in"></a>
                    <a onclick="return !window.open(this.href)" href="<?php echo get_theme_mod('redtag_link_instagram'); ?>" class="social_link ins"></a>
                </nav>
            </div>
            <span class="sub_title">Write us your question or problem. We can solve it!</span>

            <form action="/#wpcf7-f5-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                <div style="display: none;">
                    <input type="hidden" name="_wpcf7" value="5">
                    <input type="hidden" name="_wpcf7_version" value="5.0.3">
                    <input type="hidden" name="_wpcf7_locale" value="en_US">
                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f5-o1">
                    <input type="hidden" name="_wpcf7_container_post" value="0">
                </div>
                <input placeholder="Name" type="text" name="your-name" value="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" >
                <input placeholder="Email" type="email" name="your-email" value="" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false">
                <textarea placeholder="Message" name="your-message" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
                <div class="buttons">
                    <input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit btn">
                </div>
                <span class="ajax-loader"></span>
                <div class="wpcf7-response-output wpcf7-display-none"></div>
            </form>

        </div>
    </div>
</section>
