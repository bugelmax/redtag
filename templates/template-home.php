<section class="main_banner">
    <div class="container">
        <div class="main_banner__img">
            <img src="<?=get_template_directory_uri()?>/public/img/main-banner/main-banner.png" alt="">
        </div>

        <nav class="main_banner__text">
            <h3 class="title">We <span class="red">highlight</span> your business</h3>
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
                <defs>
                    <filter id="squiggly-0">
                        <feTurbulence id="turbulence" baseFrequency="0.02" numOctaves="3" result="noise" seed="0"/>
                        <feDisplacementMap id="displacement" in="SourceGraphic" in2="noise" scale="6" />
                    </filter>
                    <filter id="squiggly-1">
                        <feTurbulence id="turbulence" baseFrequency="0.02" numOctaves="3" result="noise" seed="1"/>
                        <feDisplacementMap in="SourceGraphic" in2="noise" scale="8" />
                    </filter>

                    <filter id="squiggly-2">
                        <feTurbulence id="turbulence" baseFrequency="0.02" numOctaves="3" result="noise" seed="2"/>
                        <feDisplacementMap in="SourceGraphic" in2="noise" scale="6" />
                    </filter>
                    <filter id="squiggly-3">
                        <feTurbulence id="turbulence" baseFrequency="0.02" numOctaves="3" result="noise" seed="3"/>
                        <feDisplacementMap in="SourceGraphic" in2="noise" scale="8" />
                    </filter>

                    <filter id="squiggly-4">
                        <feTurbulence id="turbulence" baseFrequency="0.02" numOctaves="3" result="noise" seed="4"/>
                        <feDisplacementMap in="SourceGraphic" in2="noise" scale="6" />
                    </filter>
                </defs>
            </svg>
            <nav class="our_skills">
                <?php
                $categories = get_categories(array('taxonomy'=>'category','hide_empty'=>false));
                if($categories){
                    foreach($categories as $cat){?>
                    <a class="our_skills__item <?php echo $cat->slug;?>" href="<?php echo ($cat->term_id==15)?'/projects':get_category_link($cat->term_id);?>">
                        <div class="img"></div>

                        <span class="our_skills__item_title"><?php echo $cat->name;?></span>
                    </a>
                    <?php }?>
                <?php }?>
            </nav>
        </div>
    </div>
</section>

<section class="about wow animated fadeInUp">
    <div class="container">
        <div class="title_block">
            <h2 class="title_text">About <br/>Company</h2>
            <span class="we_are">who <span class="red animated fadeIn">we</span> are</span>
        </div>
        <div class="block_text">
            <span class="parag"><?php the_field('about'); ?></span>
            <div class="buttons">
                <a href="/career" class="btn black wow animated fadeInLeft">Join our team</a>
                <a href="/about" class="btn white wow animated fadeInRightBig">Learn more</a>
            </div>
        </div>
    </div>
</section>

<section class="count wow animated fadeIn">
    <div class="container">
        <ul class="numbers_line wow fadeInLeft animated">
            <?php if( have_rows('count')):
                while( have_rows('count')): the_row(); ?>
                    <li>
                        <h3 class="numbers timer count-title count-number" value="<?php the_sub_field('count_value'); ?>" data-to="157" data-speed="1500">0</h3>
                        <span class="text"><?php the_sub_field('count_text'); ?></span>
                    </li>
                <?php endwhile;
            endif ;?>
        </ul>
    </div>
</section>

<section class="clients wow animated fadeIn">
    <div class="container wow fadeInRight animated">
        <div class="title_block">
            <h2 class="title_text">Worldwide <br/>Clients</h2>
            <span class="we_are">who <span class="red">we</span> are</span>
        </div>
        <div class="block_text">
            <span class="parag"><?php the_field('work_process_text'); ?></span>
        </div>
        <div class="mapcontainer">
            <div class="map">
            </div>
        </div>
    </div>
</section>

<section class="process wow animated fadeIn">
    <div class="wrap wow animated fadeInLeft"></div>
    <div class="container wow fadeInRight animated">
        <div class="title_block">
            <h2 class="title_text">work <br/>process</h2>
            <span class="we_are">we help <span class="red fadeOut default fadeIn">you</span></span>
        </div>
        <div class="work_process">
            <div class="work_process__team">
                <?php if( have_rows('work_process__team')):
                    while ( have_rows('work_process__team')) : the_row(); ?>
                        <div class="work_process__team_item">
                            <span class="title"><?php the_sub_field('work_process__team_title'); ?></span>
                            <span class="text"><?php the_sub_field('work_process__team_text'); ?></span>
                        </div>
                    <?php endwhile;
                endif; ?>
            </div>
            <ul class="work_process__steps">
                <?php if (have_rows('work_process__steps')):
                    while ( have_rows( 'work_process__steps')): the_row(); ?>
                        <li class="work_process__steps_item">
                            <img src="<?php the_sub_field('work_process__steps_image'); ?>" alt="<?php the_sub_field('work_process__steps_title'); ?>">
                            <span class="title"><?php the_sub_field('work_process__steps_title'); ?></span>
                        </li>
                    <?php endwhile;
                endif; ?>
            </ul>
        </div>
    </div>
</section>

<!--<section class="logo_clients animated">-->
<!--    <div class="container">-->
<!---->
<!--        <div class="logo_slider">-->
<!---->
<!--            --><?php //if (have_rows('gallery')):
//                while ( have_rows( 'gallery')): the_row(); ?>
<!--                    <div class="logo_slider__item">-->
<!--                        <img src="--><?php //the_sub_field('image'); ?><!--" alt="">-->
<!--                    </div>-->
<!--                --><?php //endwhile;
//            endif; ?>
<!--        </div>-->
<!--    </div>-->
<!--</section>-->

<section class="contacts wow animated fadeInUp">
    <div class="container">
        <div class="title_block">
            <h2 class="title_text">our <br/>contacts</h2>
            <span class="we_are">who <span class="red">we</span> are</span>
        </div>
        <div class="block_text">
            <div class="our_contact">
                <div class="physical_contact">
                    <span class="sub_title">Where to find us</span>
                    <div>
                        <span class="physical_contact__title">Adress:</span>
                        <div class="parag">
                            <span><?php echo get_theme_mod('redtag_adress_Ukraine', '/'); ?></span>
                            <span><?php echo get_theme_mod('redtag_adress_Netherlands', '/'); ?></span>
                        </div>
                        <span class="physical_contact__title">Email:</span>
                        <a class="parag" href="mailto:<?php echo get_theme_mod('redtag_email', '/'); ?>"><?php echo get_theme_mod('redtag_email', '/'); ?></a>
                        <span class="physical_contact__title">Phone:</span>
                        <div class="parag">
                            <a class=""  href="tel:<?php echo get_theme_mod('redtag_phone_ukr', '/'); ?>">
                                <?php echo get_theme_mod('redtag_phone_ukr','/'); ?>
                            </a>
                            <a class=""  href="tel:<?php echo get_theme_mod('redtag_phone_nd', '/'); ?>">
                                <?php echo get_theme_mod('redtag_phone_nd','/'); ?>
                            </a>
                        </div>
                    </div>
                </div>
                <nav class="social_contact">
                    <a onclick="return !window.open(this.href)" href="<?php echo get_theme_mod('redtag_link_facebook'); ?>" class="social_link fb"></a>
                    <a onclick="return !window.open(this.href)" href="<?php echo get_theme_mod('redtag_link_linkedin'); ?>" class="social_link in"></a>
                    <a onclick="return !window.open(this.href)" href="<?php echo get_theme_mod('redtag_link_instagram'); ?>" class="social_link ins"></a>
                </nav>
            </div>
            <span class="sub_title">Write us your question or problem. We can solve it!</span>

            <form action="/#wpcf7-f5-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                <div style="display: none;">
                    <input type="hidden" name="_wpcf7" value="5">
                    <input type="hidden" name="_wpcf7_version" value="5.0.3">
                    <input type="hidden" name="_wpcf7_locale" value="en_US">
                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f5-o1">
                    <input type="hidden" name="_wpcf7_container_post" value="0">
                </div>
                <input placeholder="Name" type="text" name="your-name" value="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" >
                <input placeholder="Email" type="email" name="your-email" value="" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false">
                <textarea placeholder="Message" name="your-message" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
                <div class="buttons">
                    <input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit btn">
                </div>
                <span class="ajax-loader"></span>
                <div class="wpcf7-response-output wpcf7-display-none"></div>
            </form>

        </div>
    </div>
</section>
