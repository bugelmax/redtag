<section class="contact join">
    <div class="container">
        <div class="title_block">
            <h2 class="title_text">Join Our <br/>Team</h2>
            <span class="we_are">who <span class="red">we</span> are</span>
        </div>
        <div class="block_text">
            <div class="our_contact">
                <div class="vacancies">
                    <span class="sub_title">Find vacancies here:</span>
                    <div class="buttons"><a onclick="return !window.open(this.href)" href="<?php echo get_theme_mod('redtag_link_dou'); ?>" class="btn">DOU</a></div>
                </div>
                <div class="socials">
                    <span class="sub_title">Find us in social networks:</span>
                    <nav class="social_contact">
                        <a onclick="return !window.open(this.href)" href="<?php echo get_theme_mod('redtag_link_facebook'); ?>" class="social_link fb"></a>
                        <a onclick="return !window.open(this.href)" href="<?php echo get_theme_mod('redtag_link_linkedin'); ?>" class="social_link in"></a>
                        <a onclick="return !window.open(this.href)" href="<?php echo get_theme_mod('redtag_link_instagram'); ?>" class="social_link ins"></a>
                    </nav>
                </div>
            </div>
            <div class="">
                <span class="sub_title">Write us right now:</span>
                <div role="form" class="wpcf7" id="wpcf7-f203-o1" lang="en-US" dir="ltr">
                    <div class="screen-reader-response"></div>
                    <form action="<?php echo get_permalink(); ?>#wpcf7-f203-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                        <div style="display: none;">
                            <input type="hidden" name="_wpcf7" value="203" />
                            <input type="hidden" name="_wpcf7_version" value="5.0.3" />
                            <input type="hidden" name="_wpcf7_locale" value="en_US" />
                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f203-o1" />
                            <input type="hidden" name="_wpcf7_container_post" value="0" />
                        </div>
                        <input placeholder="Name" type="text" name="your-name" value="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                        <input placeholder="Email" type="email" name="your-email" value="" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
                        <textarea placeholder="Message" name="your-message" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
                        <div class="buttons">
                            <input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit btn" />
                        </div>
                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="offer awards">
    <div class="container">
        <div class="title_block wow animated fadeInUp">
            <h2 class="title_text">what WE <br/>offer</h2>
            <span class="we_are">why <span class="red">we</span>.</span>
        </div>
        <div class="block_text">
            <span class="parag"><?php the_field('our_offer'); ?></span>
        </div>
        <div class="block_img">
            <div class="wow animated fadeInDownBig">
                <img src="<?php the_field('gallery_image_1'); ?>" alt="">
            </div>
            <div class="wow animated fadeInRight"  >
                <img src="<?php the_field('gallery_image_2'); ?>" alt="">
            </div>
            <div class="wow animated fadeInLeft"   >
                <img src="<?php the_field('gallery_image_3'); ?>" alt="">
            </div>
            <div class="wow animated fadeInUp"     >
                <img src="<?php the_field('gallery_image_4'); ?>" alt="">
            </div>
            <div class="wow animated fadeInRight"  >
                <img src="<?php the_field('gallery_image_5'); ?>" alt="">
            </div>
        </div>
    </div>
</section>

<section class="contacts place">
    <div class="container">
        <div class="title_block">
            <h2 class="title_text">our <br/>contacts</h2>
            <span class="we_are">who <span class="red">we</span> are</span>
        </div>
        <div class="block_text">
            <div class="our_contact">
                <div class="physical_contact">
                    <span class="sub_title">Where to find us</span>
                    <div>
                        <span class="physical_contact__title">Adress:</span>
                        <div class="parag adress">
                            <span><?php echo get_theme_mod('redtag_adress_Ukraine', '/'); ?></span>
                            <hr>
                            <span><?php echo get_theme_mod('redtag_adress_Netherlands', '/'); ?></span>
                        </div>
                        <span class="physical_contact__title">Email:</span>
                        <a class="parag" href="mailto:<?php echo get_theme_mod('redtag_email', '/'); ?>"><?php echo get_theme_mod('redtag_email', '/'); ?></a>
                        <span class="physical_contact__title">Phone:</span>
                        <div class="parag">
                            <a class=""  href="tel:<?php echo get_theme_mod('redtag_phone_ukr', '/'); ?>">
                                <?php echo get_theme_mod('redtag_phone_ukr','/'); ?>
                            </a>
                            <a class=""  href="tel:<?php echo get_theme_mod('redtag_phone_nd', '/'); ?>">
                                <?php echo get_theme_mod('redtag_phone_nd','/'); ?>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="place_location">
                    <?php echo do_shortcode('[wpgmza id="1"]'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
