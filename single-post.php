<?php get_header();
$template_url=get_template_directory_uri();
the_post();

?>

    <section class="single_project projects_list">
        <div class="container">
            <div class="single_project__banner">
                <div class="return"><a href="/projects"></a></div>
                <img src="<?php echo GetImageUrl(get_post_meta(get_the_ID(),'main_banner',true)); ?>" alt="">
            </div>
            <div class="description_body">
                <span class="projects_list__item_title"><?php the_title();?></span>
                <span class="projects_list__item_subtitle"><? the_content(); ?></span>
                <?php if( get_field('link') ): ?>
                    <div class="buttons">
                        <a onclick="return !window.open(this.href)" href="<?php the_field('link'); ?>" class="btn">Visit Website</a>
                    </div>
                <?php endif; ?>
                <div class="short_description">
                    <?php if( have_rows('project_description') ):
                        while ( have_rows('project_description') ) : the_row(); ?>
                            <span class="title"><?php the_sub_field('title'); ?></span>
                            <span class="descr"><?php the_sub_field('info'); ?></span>
                        <?php endwhile;
                    endif;
                    ?>
                </div>
                <?php
                $tags = get_the_tags();
                $html = '<div class="projects_list__item_tags">';
                foreach ($tags as $tag){
                    $tag_link = get_tag_link($tag->term_id);

                    $html .= "<a href='{$tag_link}' title='{$tag->name} Tag' class='projects_list__item_tags_name {$tag->slug}'>";
                    $html .= "{$tag->name}</a>";
                }
                $html .= '</div>';
                echo $html;
                ?>
            </div>
        </div>
    </section>

    <section class="count wow animated fadeIn">
        <div class="container">
            <ul class="numbers_line wow fadeInLeft animated">
                <?php if( have_rows('count')):
                    while( have_rows('count')): the_row(); ?>
                        <li>
                            <h3 class="numbers timer count-title count-number" value="<?php the_sub_field('count_value'); ?>" data-to="157" data-speed="1500">0</h3>
                            <span class="text"><?php the_sub_field('count_title'); ?></span>
                        </li>
                    <?php endwhile;
                endif ;?>
            </ul>
        </div>
    </section>

    <section class="project_description">
        <div class="container">
            <div class="project_description__body_task wow fadeInLeft animated">
                <span class="title">Task:</span>
                <p class="description">
                    <span><?php the_field('task'); ?></span>
                </p>
            </div>
            <div class="project_description__body_solution wow fadeInRight animated">
                <span class="title">Solution:</span>
                <p class="description">
                    <span><?php the_field('solution'); ?></span>
                </p>
            </div>
                <div class="all_devices">
                    <img src="<?php echo GetImageUrl(get_post_meta(get_the_ID(),'banner',true)); ?>" alt="">
                </div>
            <div class="buttons">
                <a class="btn" href="/projects">Back to All Projects</a>
            </div>
        </div>
    </section>
<?php get_footer(); ?>