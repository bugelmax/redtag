<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 14.08.18
 * Time: 21:05
 */

function redtag_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on redtag, use a find and replace
     * to change 'redtag' to the name of your theme in all the template files.
     */
    load_theme_textdomain( 'redtag', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    add_theme_support( 'title-tag' );

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'menu' => esc_html__( 'header-menu', 'redtag' ),
//        'menu-1' => esc_html__( 'header-menu-2', 'albero' ),
//        'footer-menu-1' => esc_html__( 'footer-menu-1', 'albero' ),
//        'footer-menu-2' => esc_html__( 'footer-menu-2', 'albero' ),
    ) );
}
add_action( 'after_setup_theme', 'redtag_setup' );

/**
 * Block Rewrite-Roles
 */
//add_filter('site_url', 'wpadmin_filter', 10, 3);
//function wpadmin_filter( $url, $path, $orig_scheme ) {
//    $old = array( "/(wp-admin)/");
//    $admin_dir = WP_ADMIN_DIR;
//    $new = array($admin_dir);
//    return preg_replace( $old, $new, $url, 1);
//}
//add_filter('site_url',  'wplogin_filter', 10, 3);
//function wplogin_filter( $url, $path, $orig_scheme ) {
//    $old  = array( "/(wp-login\.php)/");
//    $new  = array( "rt_admin");
//    return preg_replace( $old, $new, $url, 1);
//}

//add_action( 'init', 'block_wp_admin' );
//function block_wp_admin() {
//    if(strpos($_SERVER['REQUEST_URI'],'wp-admin') != false|| strpos($_SERVER['REQUEST_URI'], 'wp-login') !== false ){
//        wp_redirect( home_url().'/404' );
//        exit;
//    }
//}


function redtag_customize_register($wp_customize){
    //social links section
    $wp_customize->add_section(
        'redtag_social_options',
        array(
            'title'     => 'Social Link Options',
            'priority'  => 200
        )
    );

    //facebook
    $wp_customize->add_setting(
        'redtag_link_facebook',
        array(
            'default'     => 'https://facebook.com'
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'link_social_0',
            array(

                'label'      => __( 'Facebook Link', 'redtag' ),
                'description' => __( 'Select default link for Facebook account', 'redtag' ),
                'section'    => 'redtag_social_options',
                'type' => 'text',
                'settings'   => 'redtag_link_facebook'
            )
        )
    );

    //linkedin
    $wp_customize->add_setting(
        'redtag_link_linkedin',
        array(
            'default'     => 'https://linkedin.com'
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'link_social_1',
            array(

                'label'      => __( 'Linkedin Link', 'redtag' ),
                'description' => __( 'Select default link for Linkedin account', 'redtag' ),
                'section'    => 'redtag_social_options',
                'type' => 'text',
                'settings'   => 'redtag_link_linkedin'
            )
        )
    );

    //instagram
    $wp_customize->add_setting(
        'redtag_link_instagram',
        array(
            'default'     => 'https://instagram.com'
        )
    );

    //instagram
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'link_social_2',
            array(

                'label'      => __( 'Instagram Link', 'redtag' ),
                'description' => __( 'Select default link for Instagram account', 'redtag' ),
                'section'    => 'redtag_social_options',
                'type' => 'text',
                'settings'   => 'redtag_link_instagram'
            )
        )
    );

    //twitter
    $wp_customize->add_setting(
        'redtag_link_twitter',
        array(
            'default'     => 'https://twitter.com/'
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'link_social_3',
            array(

                'label'      => __( 'Twitter Link', 'redtag' ),
                'description' => __( 'Select default link for Twitter account', 'redtag' ),
                'section'    => 'redtag_social_options',
                'type' => 'text',
                'settings'   => 'redtag_link_twitter'
            )
        )
    );

    //dou
    $wp_customize->add_setting(
        'redtag_link_dou',
        array(
            'default'     => 'https://dou.ua/'
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'link_social_4',
            array(

                'label'      => __( 'DOU Link', 'redtag' ),
                'description' => __( 'Select default link for DOU account', 'redtag' ),
                'section'    => 'redtag_social_options',
                'type' => 'text',
                'settings'   => 'redtag_link_dou'
            )
        )
    );

    // header control section
    $wp_customize->add_section(
        'redtag_header_options',
        array(
            'title'     => 'Header Options',
            'priority'  => 200
        )
    );

    //phone number
    $wp_customize->add_setting(
        'redtag_phone_ukr',
        array(
            'default'     => '+380934860803'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'redtag_header_0',
            array(

                'label'      => __( 'Ukraine phone number', 'redtag' ),
                'description' => __( 'Контактний номер телефону в Україні', 'redtag' ),
                'section'    => 'redtag_header_options',
                'type' => 'text',
                'settings'   => 'redtag_phone_ukr'
            )
        )
    );
    $wp_customize->add_setting(
        'redtag_phone_nd',
        array(
            'default'     => '+310302021616'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'redtag_header_1',
            array(

                'label'      => __( 'Netherlands phone number', 'redtag' ),
                'description' => __( 'Контактний номер телефону в Нідерланлах', 'redtag' ),
                'section'    => 'redtag_header_options',
                'type' => 'text',
                'settings'   => 'redtag_phone_nd'
            )
        )
    );

    //email
    $wp_customize->add_setting(
        'redtag_email',
        array(
            'default'     => 'info@redtag.com.ua'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'redtag_header_2',
            array(

                'label'      => __( 'Email', 'redtag' ),
                'description' => __( 'Контактний email', 'redtag' ),
                'section'    => 'redtag_header_options',
                'type' => 'text',
                'settings'   => 'redtag_email'
            )
        )
    );

    //adress
    $wp_customize->add_setting(
        'redtag_adress_Ukraine',
        array(
            'default'     => "106, Bohdana Khmel'nyts'koho street <br> Lviv 79057 <br>Ukraine"
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'redtag_header_3',
            array(

                'label'      => __( 'Adress in Ukraine', 'redtag' ),
                'description' => __( 'Адреса компанії в Україні', 'redtag' ),
                'section'    => 'redtag_header_options',
                'type' => 'text',
                'settings'   => 'redtag_adress_Ukraine'
            )
        )
    );
    //adress
    $wp_customize->add_setting(
        'redtag_adress_Netherlands',
        array(
            'default'     => "Netherlands<br> Gruttersdijk 22 <br> 3514BG, Utrecht <br> 030 - 202 1616"
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'redtag_header_4',
            array(

                'label'      => __( 'Adress in Netherlands', 'redtag' ),
                'description' => __( 'Адреса компанії в Нідерланлах', 'redtag' ),
                'section'    => 'redtag_header_options',
                'type' => 'text',
                'settings'   => 'redtag_adress_Netherlands'
            )
        )
    );

}
add_action('customize_register', 'redtag_customize_register');

function show_image($id,$size='full',$show_baner=false){
    if ($show_baner) {
        $image=image_downsize( $id, $size );
        return str_replace('/wp-content/uploads/','/images/',$image[0]);
    }
    else
        return str_replace('/wp-content/uploads/','/images/',get_the_post_thumbnail_url( $id,$size ));
}

function do_excerpt($string, $word_limit) {
    $words = explode(' ', $string, ($word_limit + 1));
    if (count($words) > $word_limit)
        array_pop($words);
    echo implode(' ', $words).'';
}

function GetImageUrl($id=0,$size='full'){
    $temp=image_downsize($id,$size);
    return $temp[0];
}
function GetCategoriesByTaxonomy($taxonomy='category',$hide_empty = false)
{

    $categories = get_terms(array('taxonomy' => $taxonomy, 'hide_empty' => $hide_empty));

    if (is_wp_error($categories)) return false;
    if (count($categories) > 0) {
        foreach ($categories as $key => $obj) {
            $obj = (array)$obj;
            $obj['thumbnail'] = GetImageByCategory($obj["term_id"],'thumbnail_id','medium');
            $obj['url']=get_term_link($obj["term_id"]);
            /*      if ($hide_empty)
                      if (($obj['count']==0)&&(filter_var($obj["description"], FILTER_VALIDATE_URL)===false)) unset ($categories[$key]); //the same as 'hide_empty' => true
                  else*/
            $categories[$key] = (object)$obj;


        }
    } else return false;
    return $categories;
}
function GetImageByCategory($category_id=0,$key='category-image-id',$image_size='full'){
    $image_id = get_term_meta ( $category_id, $key, true );
    if ( $image_id )
        return image_downsize( $image_id ,$image_size)[0];
    else return false;
}

