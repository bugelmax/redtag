<?php wp_footer(); ?>
    <footer>
        <div class="container">
            <div class="copywrite">
                <span>© Red Tag 2013<?=(date('Y')!='2013')?' - '.date('Y'):''?></span>
            </div>
            <div class="logo">
                <a href="/"><img src="<?=get_template_directory_uri()?>/public/img/header/header_logo.svg" alt="company logo"></a>
            </div>
        </div>
    </footer>

    <div class="modal_box">
        <div class="owerlay"></div>
        <div class="modal mail_box">
            <div class="closeModal"><img src="<?=get_template_directory_uri()?>/public/img/modal/cancel-1.png" alt=""></div>
            <section class="contacts">
                <div class="block_text">
                    <span class="sub_title">Write us your question or problem.</span>
                    <div role="form" class="wpcf7" id="wpcf7-f203-o1" lang="en-US" dir="ltr">
                        <div class="screen-reader-response"></div>
                        <form action="/#wpcf7-f203-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                            <div style="display: none;">
                                <input type="hidden" name="_wpcf7" value="203" />
                                <input type="hidden" name="_wpcf7_version" value="5.0.3" />
                                <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f203-o1" />
                                <input type="hidden" name="_wpcf7_container_post" value="0" />
                            </div>
                            <input placeholder="Name" type="text" name="your-name" value="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                            <input placeholder="Email" type="email" name="your-email" value="" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
                            <textarea placeholder="Message" name="your-message" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
                            <div class="buttons">
                                <input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit btn" />
                            </div>
                            <div class="wpcf7-response-output wpcf7-display-none"></div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <div class="modal sentMessage">
            <div class="closeModal"><img src="<?=get_template_directory_uri()?>/public/img/modal/cancel-1.png" alt=""></div>
            <div class="success">
                <span class="sub_title">Your message sent successfully!</span>
                <div class="buttons"><span class="btn submit">Okay</span></div>
            </div>
        </div>
    </div>

    <script src="<?=get_template_directory_uri()?>/public/js/libs.min.js"></script>
<!--    <script src="--><?//=get_template_directory_uri()?><!--/public/js/common.min.js"></script>-->
<script src="<?=get_template_directory_uri()?>/public/js/script.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <?php
        if (is_front_page() || is_single()){ ;?>
            <script>
                function scroll_number(){
                    $('.numbers_line li').each(function() {
                        var number = $(this).children('h3');
                        var number_val = number.attr('value');
                        number.animate({ num: number_val - 0 }, {
                            duration: 5000,
                            step: function (num){
                                this.innerHTML = (num + 0).toFixed(0)
                            }
                        });
                    });
                }
                function run_numbers(){
                    var a = $(document).scrollTop();
                    var b = $('.count').offset().top - 600;
                    if(a >= b) {
                        scroll_number();
                    }
                }
                $(document).on('scroll',function(){
                    run_numbers();
                });
            </script>
    <?php }; ?>
    <script>
         // $(document).ready(function(){
         //     $('.wpcf7-form').on('submit', function(e){
         //         e.preventDefault();
         //         popup($(this));
         //     })
         // });
        function popup(obj){
            var form_submited=obj;
            form_submited.serialize();
            // console.log(form_submited.serialize());
            // return;
            $.ajax({
                // url: wc_add_to_cart_params.ajax_url,
                type: "POST",
                data:form_submited.serialize(),
                dataType: "json",
                async: true,
                cache: false,
                timeout: 30000,
                beforeSend: function (xhr) {},
                success: function (response) {
                    //if success then remove
                    try {
                        if (response.status == true){

                            obj.closest('a').remove();

                        }

                    }
                    catch (exception){}
                }
            });
        };
    </script>
<!--<script>-->
<!--    var map;-->
<!--    function initMap() {-->
<!--        map = new google.maps.Map(document.getElementById('map'), {-->
<!--            center: {lat: -34.397, lng: 150.644},-->
<!--            zoom: 8-->
<!--        });-->
<!--    }-->
<!--</script>-->
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2nFCkrLircyGbZJGPkMj-8bcvW-eaLU0&callback=initMap" async defer></script>-->
</body>
</html>
