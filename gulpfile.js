var gulp           = require('gulp'),
		gutil          	= require('gulp-util' ),
    	// del 			= require('del'),
		sass           	= require('gulp-sass'),
		browserSync    	= require('browser-sync'),
		concat         	= require('gulp-concat'),
		uglify         	= require('gulp-uglify'),
		cssnano 		= require('gulp-cssnano'),
		cleanCSS    	= require('gulp-clean-css'),
		rename         	= require('gulp-rename'),
		autoprefixer   	= require('gulp-autoprefixer'),
		notify         	= require("gulp-notify");
    	// imagemin 		= require('gulp-imagemin'),
        // pngquant 		= require('imagemin-pngquant'),
        // cache 			= require('gulp-cache');

gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'app'
		},
		notify: false,
	});
});


gulp.task('js-libs', function() {
	return gulp.src([
        'app/libs/jquery.min.js',
        'app/libs/slick.min.js',
        'app/libs/jquery.mousewheel.min.js',
        'app/libs/raphael.min.js',
        'app/libs/jquery.mapael.min.js',
        'app/libs/world_countries.min.js',
        'app/libs/wow.min.js'
		])
	.pipe(concat('libs.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('app/js'))
	.pipe(browserSync.reload({stream: true}));
});
gulp.task('js', function() {
	return gulp.src('app/js/common.js')
	.pipe(concat('common.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('app/js'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('sass', function() {
	return gulp.src('app/scss/**/*.scss')
	.pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
	.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
	.pipe(cssnano())
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('app/css'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('watch', ['sass', 'js-libs', 'js', 'browser-sync'], function() {
	gulp.watch('app/scss/**/*.scss', ['sass']);
	gulp.watch(['libs/**/*.js', 'app/js/common.js'], ['js']);
	gulp.watch('app/*.html', browserSync.reload);
});

// gulp.task('clean', function() {
//     return del.sync('public');
// });
gulp.task('clear', function (callback) {
    return cache.clearAll();
});
gulp.task('img', function() {
    return gulp.src('app/img/**/*') // Берем все изображения из app
        .pipe(cache(imagemin({ // С кешированием
            // .pipe(imagemin({ // Сжимаем изображения без кеширования
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))/**/)
        .pipe(gulp.dest('public/img')); // Выгружаем на продакшен
});

gulp.task('build', ['clean', 'img', 'sass', 'js'], function(){
    var buildCss = gulp.src('app/css/**/*.*')
        .pipe(gulp.dest('public/css'));

    var buildFonts = gulp.src('app/fonts/**/*.*')
        .pipe(gulp.dest('public/fonts'));

    var buildJs = gulp.src('app/js/**/*.min.js')
        .pipe(gulp.dest('public/js'));

    // var buildLibsJs = gulp.src('app/libs/**/*.min.js')
    //     .pipe(gulp.dest('public/js'));

    var buildHTML= gulp.src('app/*.html')
        .pipe(gulp.dest('public'));
});

gulp.task('default', ['watch']);
