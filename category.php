<?php
get_header();
$template_url=get_template_directory_uri();
$category = get_queried_object();
echo $category->term_id;
$args = array(
    'numberposts' => 13,

    'orderby'     => 'date',
    'order'       => 'DESC',
    'include'     => array(),
    'exclude'     => array(),
    'post_type'   => 'post',
    'suppress_filters' => false,
    'taxonomy' => 'category',
    'category' => $category->term_id,

);
$posts=get_posts($args);

?>

<section class="projects wow animated fadeInUp">
    <div class="container">
        <div class="title_block">
            <h2 class="title_text">Our<br/>projects</h2>
            <span class="we_are">what <span class="red">we</span> do.</span>
        </div>
        <nav class="our_skills">
            <?php
            $categories = get_categories(array('taxonomy'=>'category','hide_empty'=>false));
            if($categories){
                foreach($categories as $cat){?>
                    <a class="our_skills__item <?php echo $cat->slug;?>" href="<?php echo ($cat->term_id==15)?'/projects':get_category_link($cat->term_id);?>">
                        <div class="img"></div>
<!--                        --><?php //if($imgcat1=get_field("thumbnail",$cat)){?>
<!--                            <img class="our_skills__item_img" src="--><?php //echo $imgcat1;?><!--"/>-->
<!--                        --><?php //}?>
                        <span class="our_skills__item_title"><?php echo $cat->name;?></span>
                    </a>
                <?php }?>
            <?php }?>
        </nav>
    </div>
</section>
<section class="projects_list">
    <div class="container">
        <?php $post_counter=1; foreach ($posts as $obj):?>
            <div class="projects_list__item ">
                <div class="projects_list__item_description ">
                    <div class="projects_list__item_name">
                        <span class="projects_list__item_number">0<?=$post_counter?></span>
                        <a href="<?=get_permalink($obj->ID)?>" class="projects_list__item_title"><?php echo $obj->post_title;?></a>
                    </div>
                    <div class="projects_list__item_img ">
                        <?php echo get_the_post_thumbnail($obj->ID); ?>
                    </div>
                    <div class="projects_list__item_description">
                        <span class="projects_list__item_excerpt"><?php echo do_excerpt($obj->post_excerpt, 200); ?></span>
                    </div>
                    <?php
                    $post_tags = get_the_terms( $obj->ID, 'post_tag' );
                    $html = '<div class="projects_list__item_tags">';
                    foreach ($post_tags as $tag){
                        $tag_link = get_tag_link($tag->term_id);

                        $html .= "<a href='{$tag_link}' title='{$tag->name} Tag' class='projects_list__item_tags_name {$tag->slug}'>";
                        $html .= "{$tag->name}</a>";
                    }
                    $html .= '</div>';
                    echo $html;
                    ?>
                    <div class="buttons"><a href="<?=get_permalink($obj->ID)?>" class="btn">Learn more</a></div>
                </div>
                <div class="projects_list__item_img ">
                    <?php echo get_the_post_thumbnail($obj->ID); ?>
                </div>
            </div>

            <?php
            $post_counter++;
        endforeach;
        ?>
    </div>
</section>
<?php get_footer(); ?>
